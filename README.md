# CoFee UP

This is the CoFee Framework for providing continuous feedback on C homework to computer science students
The original publication can be found here: Leveraging Continuous Feedback in Education to Increase C Code Quality (https://ieeexplore.ieee.org/document/10216467)

## Demo 

A demo on the feedback provided to the students can be found here:

https://gitlab.com/cofee-demo/c-demo


## Structure
This module is just the glue code, that pulls all excercise data and all analysis modules into one image.
This image is than used by the pipeline to analyse students code.

Folders:

- scripts: Contains some simple management scripts to create groups or sync student groups between moodle and gitlab
- templates: Contain the jinja2 files to create the static html reports
- excercies: Contains all the needed files to test the students code for one exercise. For example the unit test files and Makefiles for specific sanitizers
- modules: Extra Modules written to analyze students code. These modules extend the Clang Static Analyzer or Clang Tidy.
- parser: The parser that parses tool output and create static html reports but also uploads all results as JUnit and PKL artifacts for later analysis by the teacher.


