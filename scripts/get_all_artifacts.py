#!/usr/bin/env python
import gitlab
import subprocess
import argparse
import csv
import os
import sys
import logging as log

testing=True

def main():

    arg_parser = argparse.ArgumentParser(
        description='Tool to download and update all student repositories in [dir]')
    arg_parser.add_argument(
        'dir',
        type=str,
        help='Destination for all repositories')
    arg_parser.add_argument(
        '--dryrun',
        action='store_true',
        help='does not change anything and prints only actions')
    args = arg_parser.parse_args()
    destination=os.path.abspath(args.dir)
    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    global testing 
    testing=args.dryrun
    gl = gitlab.Gitlab(url="https://gitup.uni-potsdam.de", private_token=gitlab_token)
    gbr2022 = gl.groups.get(10470)
    subgroups=gbr2022.subgroups.list(all=True)
    print("Create/Ensure Directory:" + destination,file=sys.stderr)
    if not testing:
        os.makedirs(destination, exist_ok=True)
    for subgroup in subgroups:
        print("Create/Ensure Group Directory:" + destination +'/'+subgroup.name ,file=sys.stderr)
        if not testing:
            os.makedirs(destination+'/'+subgroup.name, exist_ok=True)
        group = gl.groups.get(subgroup.id)
        projects = group.projects.list()
        for project in projects:
            url='https://downloadAPI:'+gitlab_token+'@'+project.http_url_to_repo[8:]
            project_path=destination+'/'+subgroup.name+'/'+project.path
            if not os.path.isdir(project_path):
                print("Create Repositorie dir:" , url , '\t into:' , project_path ,file=sys.stderr)
                if not testing:
                    os.makedirs(project_path, exist_ok=True)
            project_with_job_list =  gl.projects.get(project.id)
            for job in project_with_job_list.jobs.list(all=True):
                job_path=project_path+'/'+str(job.id)
                if not os.path.exists(job_path):
                    print("Create job path:", job_path ,file=sys.stderr)
                    if not testing:
                        os.makedirs(job_path, exist_ok=True)
                    print("Download artifact",file=sys.stderr)
                    if not testing:
                        try:
                            zipfn = job_path+"/___artifacts.zip"
                            with open(zipfn, "wb") as f:
                                job.artifacts(streamed=True, action=f.write)
                            subprocess.Popen(["unzip", "-bo", zipfn],cwd=job_path)
                        except:
                            pass

if __name__ == "__main__":
   main()
