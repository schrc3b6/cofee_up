import argparse
import csv

import gitlab
import logging as log
import os

testing = True
user_not_to_remove = ["schnor"]


def get_members_not_to_remove(gl, parent):
    global user_not_to_remove
    l = []
    members = parent.members.list()
    for member in members:
        l.append(member.username)
    return l + user_not_to_remove


# Parsing csv
def get_studis_from_csv(file: str):
    studis = []
    with open(file, mode="r") as f:
        csvFile = csv.reader(f)
        for line in csvFile:
            if "@" in line[2]:
                studi = dict()
                studi["login"] = line[2].split("@")[0]
                studi["name"] = (line[0] + " " + line[1]).replace('"', "")
                studis.append(studi)
    return studis


def get_gitlab_uid(gl, studis):
    # Trying to find students in gitlab
    for studi in list(studis):
        user = gl.users.list(username=studi["login"])
        if len(user) == 1:
            # print(studi,user)
            studi["userid"] = user[0].id
            studi["gitlab_username"] = user[0].username
        else:
            user = gl.users.list(search=studi["name"])
            if len(user) == 1:
                # print(studi,user)
                studi["userid"] = user[0].id
                studi["gitlab_username"] = user[0].username
                pass
            else:
                print("Not Found in Gitlab:", studi)
                studis.remove(studi)


def create_subgroups(gl, parent, studis):
    for studi in studis:
        print(
            f"create subgroup for {studi['name']} with name {studi['name']} and path {studi['login'].lower()} and adding {studi['userid']}"
        )
        if not testing:
            try:
                subgroup = gl.groups.create(
                    {
                        "name": studi["name"],
                        "path": studi["login"].lower(),
                        "parent_id": parent.id,
                        "visibility": "private",
                        "request_access_enabled": "false",
                    }
                )
            except Exception as e:
                print("Couldn't Create Subgroup for ", studi["name"], e)
            try:
                subgroup = gl.groups.list(search=studi["login"].lower())
                if len(subgroup) == 1:
                    subgroup = subgroup[0]
                else:
                    print("Couldn't find Subgroup for ", studi["name"])
                    raise Exception("Couldn't find Subgroup for ", studi["name"])
                subgroup.members.create(
                    {
                        "user_id": studi["userid"],
                        "access_level": gitlab.const.AccessLevel.DEVELOPER,
                    }
                )
            except Exception as e:
                print("Couldn't add the following studi to the group as DEVELOPER:", studi["name"], e)
            try:
                subgroup = gl.groups.list(search=studi["login"].lower())
                if len(subgroup) == 1:
                    subgroup = subgroup[0]
                else:
                    print("Couldn't find Subgroup for ", studi["name"])
                    raise Exception("Couldn't find Subgroup for ", studi["name"])
                gl.projects.get(8591).forks.create(
                    {"namespace_id": subgroup.id}
                )
            except Exception as e:
                print("Couldn't fork the project for the studi", studi["name"], e)


def main():
    arg_parser = argparse.ArgumentParser(
        description="Tool to add Moodle Students to Git"
    )
    arg_parser.add_argument("csv", type=str, help="Moodle BewertungsCSV")
    arg_parser.add_argument(
        "--dryrun",
        action="store_true",
        help='The output file e.g.: "output.xml" - it is a junit.xml',
    )
    args = arg_parser.parse_args()

    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    global testing
    testing = args.dryrun
    studis = get_studis_from_csv(args.csv)
    gl = gitlab.Gitlab(
        url="https://gitup.uni-potsdam.de",
        private_token=gitlab_token,
    )
    get_gitlab_uid(gl=gl, studis=studis)
    gbr2022 = gl.groups.get(12434)
    create_subgroups(gl=gl, parent=gbr2022, studis=studis)
    pass


if __name__ == "__main__":
    main()
