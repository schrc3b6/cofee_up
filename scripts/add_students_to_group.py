import gitlab
import argparse
import csv
import os
import logging as log

testing=True
user_not_to_remove = ['schnor']

def get_members_not_to_remove(gl,parent):
    global user_not_to_remove
    l=[]
    members= parent.members.list()
    for member in members:
        l.append(member.username)
    return l + user_not_to_remove


#Parsing csv
def get_group_members_from_csv(file:str):
    studis=[]
    with open(file, mode ='r')as file:
        csvFile = csv.reader(file)
        for line in csvFile:
            if "@" in line[3]:
                studi= dict()
                studi['login']=line[3].split('@')[0]
                studi['name']=line[1].split('(')[0].strip()
                studi['gruppe']=line[6]
                if "Kein Mitglied" in line[6]:
                    print("Kein Mitglied einer Gruppe", studi['login'],studi['name'] )
                    continue
                studis.append(studi)
    return studis


def get_gitlab_uid(gl,studis):
# Trying to find students in gitlab
    for studi in list(studis):
        user=gl.users.list(username=studi['login'])
        if (len(user)==1):
            # print(studi,user)
            studi['userid']=user[0].id
            studi['gitlab_username']=user[0].username
        else:
            user=gl.users.list(search=studi['name'])
            if (len(user)==1):
                # print(studi,user)
                studi['userid']=user[0].id
                studi['gitlab_username']=user[0].username
                pass
            else:
                print("Not Found in Gitlab:", studi)
                studis.remove(studi)
        
def remove_students_from_groups(gl,gruppen,studis,not_remove):
# Removing Students from Gitlab
    for subgruppe in gruppen:
        subid=subgruppe.id
        gruppe = gl.groups.get(subid)
        members= gruppe.members.list()
        for member in members:
            if member.username in not_remove:
                continue
            for studi in studis:
                if member.username == studi['gitlab_username']:
                    if studi['gruppe']!= subgruppe.name:
                        print("Student", studi, "deleted from subgroup", subgruppe.name)
                        if not testing:
                            member.delete()

def add_students_to_group(gl,gruppen,studis):
# Adding Students to Gitlab
    for studi in studis:
        if "Nicht Mitglied" in studi['gruppe']:
            continue;
        found=False
        for gruppe in gruppen:
            if gruppe.name == studi['gruppe']:
                found=True
                subid=gruppe.id
                break
        if not found:
            print("Group not found")
            continue
        gruppe = gl.groups.get(subid)
        members= gruppe.members.list()
        not_member=True
        for member in members:
            if member.username == studi['gitlab_username']:
                not_member=False
        if not_member:
            print("Student", studi, "added to subgroup", gruppe.name)
            if not testing:
                member = gruppe.members.create({'user_id': studi['userid'], 'access_level': gitlab.const.AccessLevel.MAINTAINER})

def main():

    arg_parser = argparse.ArgumentParser(
        description='Tool to add Moodle Students to Git')
    arg_parser.add_argument(
        'csv',
        type=str,
        help='Moodle BewertungsCSV')
    arg_parser.add_argument(
        '--dryrun',
        action='store_true',
        help='The output file e.g.: "output.xml" - it is a junit.xml')
    args = arg_parser.parse_args()
    gitlab_token = os.getenv('GITLAB_TOKEN', None)
    if gitlab_token is None:
        log.error("Please set the GITLAB_TOKEN environment variable")
        exit(1)
    global testing
    print("TOKEN:", gitlab_token)
    testing=args.dryrun
    studis= get_group_members_from_csv(args.csv)
    print()
    gl = gitlab.Gitlab(url="https://gitup.uni-potsdam.de", private_token=gitlab_token)
    get_gitlab_uid(gl=gl,studis=studis)
    print()
    gbr2022 = gl.groups.get(16763)
    gruppen=gbr2022.subgroups.list(all=True)
    dont_remove = get_members_not_to_remove(gl, gbr2022)
    remove_students_from_groups(gl=gl,gruppen=gruppen,studis=studis,not_remove=dont_remove)
    add_students_to_group(gl=gl,gruppen=gruppen,studis=studis)

    pass

if __name__ == "__main__":
   main()
